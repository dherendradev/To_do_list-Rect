import React from 'react';
import './App.css';

const List = props=>(
  <ul>
  { 
    props.items.map((index,item)=> <li key={index}>{item}</li>)
  }
  </ul>
  
)

class  App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      term : '',
      items: []
    }
  };
  onChange = (event)=>{
      this.setState({
      items : event.target.value
      })
    }
  
  onSubmit=(event)=>{
    event.preventDefault();
    this.setState({
      term: '',
      items : [...this.state.items, this.state.term]
    })
  }

  render(){
    return (
      <div className="App">
        <form onSubmit = {this.onSubmit}>
          <input value={this.state.term} onChange = {this.onChange}></input>
          <button>Submit</button>
        </form>
        <List items ={this.state.items}></List>
      </div>
    );
  }
}
  

export default App;
